package ru.startandroid.denis.home.twoactivity;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Denis on 04.07.2016.
 */

public class ActivityTwo extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two);
    }
}
