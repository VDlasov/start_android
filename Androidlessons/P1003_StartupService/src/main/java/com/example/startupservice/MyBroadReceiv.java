package com.example.startupservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Denis on 15.09.2016.
 */

public class MyBroadReceiv extends BroadcastReceiver {
    final String LOG_TAG = MyBroadReceiv.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_TAG, "onReceive " + intent.getAction());
        context.startService(new Intent(context, MyService.class));
    }
}
