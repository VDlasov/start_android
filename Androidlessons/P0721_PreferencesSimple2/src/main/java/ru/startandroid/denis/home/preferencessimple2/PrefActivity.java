package ru.startandroid.denis.home.preferencessimple2;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Denis on 03.08.2016.
 */

public class PrefActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
    }
}
