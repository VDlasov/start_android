package ru.startandroid.denis.home.actionbaritems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;

/**
 * Created by Denis on 28.09.2016.
 */

public class SecondActivity extends AppCompatActivity implements ActionBar.OnNavigationListener {

    // TODO: Didn't working as expected

    final String LOG_TAG = "myLogs";
    String[] data = new String[]{"one", "two", "three"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar bar = getSupportActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bar.setListNavigationCallbacks(adapter, this);
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        Log.d(LOG_TAG, "selected: position " + itemPosition + ", id = " + itemId + ", " + data[itemPosition]);
        return false;
    }
}
