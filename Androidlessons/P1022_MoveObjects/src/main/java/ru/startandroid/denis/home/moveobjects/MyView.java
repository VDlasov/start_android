package ru.startandroid.denis.home.moveobjects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Denis on 20.09.2016.
 */
public class MyView extends View {
    private static final String LOG_TAG = MyView.class.getSimpleName();
    Paint p;
    // координаты для рисованяи квадрата
    float x = 100;
    float y = 100;
    int side = 100;

    // переменные для перетаскивания
    boolean drag = false;
    float dragX = 0;
    float dragY = 0;

    public MyView(Context context) {
        super(context);
        p = new Paint();
        p.setColor(Color.GREEN);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // рисуем квадарт
        canvas.drawRect(x, y, x + side, y + side, p);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Координаты Touch события
        float evX = event.getX();
        float evY = event.getY();

        switch (event.getAction()) {
            // касание началось
            case MotionEvent.ACTION_DOWN: {
                // если касание было начато в пределах квадрата
                if (evX >= x && evX < x + side && evY >= y && evY < y + side) {
                    // включаем режим перетаскивания
                    drag = true;
                    // разница между левым верхним углом квадрата и точкой касания
                    dragX = evX - x;
                    dragY = evY - y;
                    Log.d(LOG_TAG, "onTouchEvent: " + dragX + ", " + evX + ", " + x);
                    Log.d(LOG_TAG, "onTouchEvent: " + dragY + ", " + evY + ", " + y);
                }
            }
            break;
            // тащим
            case MotionEvent.ACTION_MOVE: {
                // если режим перетаскивания включен
                if (drag) {
                    // определяем новые координаты для рисования
                    x = evX - dragX;
                    y = evY - dragY;
                    // перерисовываем экран
                    invalidate();
                }
                break;
            }
            // касание завершено
            case MotionEvent.ACTION_UP: {
                // выключаем режим перетаскивания
                drag = false;
                break;
            }
        }
        return true;
    }
}
