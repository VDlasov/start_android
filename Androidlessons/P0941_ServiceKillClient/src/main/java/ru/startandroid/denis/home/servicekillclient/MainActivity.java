package ru.startandroid.denis.home.servicekillclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickStart(View v) {
        // Android 5.0 (L) Service Intent must be explicit, not implicit
        Intent serviceIntent = new Intent("ru.startandroid.denis.home.servicekillserver.MyService")
                .setPackage("ru.startandroid.denis.home.servicekillserver")
                .putExtra("name", "value");
        startService(serviceIntent);
    }
}
