package ru.startandroid.denis.home.tabintent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Denis on 10.08.2016.
 */

public class TwoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two);
    }
}
