package ru.startandroid.denis.home.customadapter;

/**
 * Created by Denis on 16.07.2016.
 */

public class Product {
    String name;
    int price;
    int image;
    boolean box;

    Product(String _describe, int _price, int _image, boolean _box) {
        name = _describe;
        price = _price;
        image = _image;
        box = _box;
    }
}
