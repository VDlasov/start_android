package com.example.intentservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void StarService(View view) throws InterruptedException {
        Intent intent = new Intent(this, MyService.class);
        startService(intent.putExtra("time", 3).putExtra("label", "Call 1") );
        startService(intent.putExtra("time", 1).putExtra("label", "Call 2") );
        startService(intent.putExtra("time", 4).putExtra("label", "Call 3") );
    }
}
